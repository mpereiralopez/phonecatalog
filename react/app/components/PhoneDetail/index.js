import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Carousel, List, Alert } from 'antd';
import './style.css';

function generateSlides(imgs) {
  let slides = [];
  if (imgs && imgs.length > 0) {
    slides = imgs.map((img) => <div className="img-container" key={img.split('/').pop()}><img className="image" alt={img.split('/').pop()} src={img} /></div>);
  }
  return slides;
}

export default function PhoneDetailComponent(props) {
  const {
    id,
    labels,
    imgs,
    deviceName,
    brand,
    os,
    dimensions,
    weight,
    sim,
    resolution,
    technology,
    wlan,
    bluetooth,
    usb,
    battery_c,
    primary_,
    video,
    secondary,
    speed,
    network_c,
    chipset,
  } = props;
  const phoneImages = generateSlides(imgs);
  return (
    <section className="detail-holder">

      {!id &&
        <Alert
          message={<FormattedMessage {...labels.alert_message} />}
          description={<FormattedMessage {...labels.alert_description} />}
          type="info"
          showIcon
        />}

      {id &&
        <div>
          <div className="detail-margined">
            <Carousel autoplay>
              {phoneImages}
            </Carousel>
          </div>
          <List
            bordered
            dataSource={[{ deviceName }, { brand }, { os }, { dimensions }, { weight }, { sim }, { resolution },
            { technology }, { wlan }, { bluetooth }, { usb }, { battery_c }, { primary_ }, { video },
            { secondary }, { speed }, { network_c }, { chipset }]}
            renderItem={(item) => {
              const key = Object.keys(item)[0];
              return (<List.Item><b><FormattedMessage {...labels[key]} />:&nbsp;</b>{item[key]}</List.Item>);
            }}
          />
        </div>}
    </section >

  );
}

PhoneDetailComponent.propTypes = {
  id: PropTypes.number,
  labels: PropTypes.object,
  imgs: PropTypes.arrayOf(PropTypes.string),
  deviceName: PropTypes.string,
  brand: PropTypes.string,
  os: PropTypes.string,
  dimensions: PropTypes.string,
  weight: PropTypes.string,
  sim: PropTypes.string,
  resolution: PropTypes.string,
  technology: PropTypes.string,
  wlan: PropTypes.string,
  bluetooth: PropTypes.string,
  usb: PropTypes.string,
  battery_c: PropTypes.string,
  primary_: PropTypes.string,
  video: PropTypes.string,
  secondary: PropTypes.string,
  speed: PropTypes.string,
  network_c: PropTypes.string,
  chipset: PropTypes.string,
};
