import React from 'react';
import { Spin } from 'antd';
import { FormattedMessage } from 'react-intl';

import './style.css';

export default function LoadingSpinner(msg) {
  return (<div className="spinnerContainer">
    <h2><FormattedMessage {...msg} /></h2>
    <Spin size="large" />
  </div>);
}
