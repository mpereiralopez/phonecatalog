import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Card, Icon, List } from 'antd';
import './style.css';

export default function PhoneCardComponent(props) {
  const phoneData = { ...props };
  const { labels, id, imgs, deviceName, brand, os, dimensions, weight, sim, resolution } = phoneData;

  return (
    <section>
      <Card
        cover={<img className="avatar-image" alt="example" src={imgs && imgs[0]} />}
        actions={[<Icon type="search" onClick={() => { props.viewPhoneDetail(id); }} />]}
      >
        <List
          size="small"
          bordered
          dataSource={[{ deviceName }, { brand }, { os }, { dimensions }, { weight }, { sim }, { resolution }]}
          renderItem={(item) => {
            const key = Object.keys(item)[0];
            return (<List.Item><b><FormattedMessage {...labels[key]} />: </b>{item[key]}</List.Item>);
          }}
        />
      </Card>
    </section >
  );
}

PhoneCardComponent.propTypes = {
  viewPhoneDetail: PropTypes.func,
};
