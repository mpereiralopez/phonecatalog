/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Layout } from 'antd';
import messages from './messages';
const { Header, Content } = Layout;

export default class NotFound extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Layout style={{ height: '100vh' }}>
        <Header>
          <h1 style={{ color: 'white' }}>
            <FormattedMessage {...messages.header} />
          </h1>
        </Header>
        <Content style={{ margin: '15px' }}>
          <div>
            <h2><FormattedMessage {...messages.description} /></h2>
          </div>
        </Content>
      </Layout>
    );
  }
}
