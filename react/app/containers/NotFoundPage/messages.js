/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.NotFoundPage.header',
    defaultMessage: '404!',
  },
  description: {
    id: 'app.components.NotFoundPage.description',
    defaultMessage: 'Donde estas intentdo ir?',
  },
});
