import axios from 'axios';

export const fetchFromApi = async (url) => {
  try {
    const response = await axios.get(url);
    const responseBody = response.data;
    return responseBody;
  } catch (error) {
    throw error;
  }
};
