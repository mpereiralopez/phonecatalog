import { fromJS } from 'immutable';
import { actions } from './../constants';

const initialState = fromJS({ phone: {}, loading: true, hasError: false, errorMsg: '' });


export default function (state = initialState, action) {
  switch (action.type) {
    case actions.PHONE_DETAIL_FETCH_REQUEST:
      return state.set('loading', true).set('phone', {});
    case actions.PHONE_DETAIL_FETCH_SUCCESS:
      return state.set('loading', false).set('phone', (action.phone !== '') ? action.phone : {});
    case actions.PHONE_DETAIL_FETCH_ERROR:
      return state.set('loading', false).set('phone', {}).set('hasError', true).set('errorMsg', `${action.error}`);
    default:
      return state;
  }
}
