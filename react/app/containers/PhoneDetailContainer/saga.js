import { takeLatest, call, put } from 'redux-saga/effects';
import { actions, CONN } from './../constants';
import { fetchFromApi } from './../feed';


const { API_BASE_URL, API_PORT, API_VERSION, PHONES } = CONN;

export function* fetchPhoneDetail(req) {
  const { id } = req;
  try {
    const url = `${API_BASE_URL}:${API_PORT}/${API_VERSION}/${PHONES}/${id}`;
    const result = yield call(() => fetchFromApi(url));
    yield put({ type: actions.PHONE_DETAIL_FETCH_SUCCESS, phone: result });
  } catch (e) {
    yield put({ type: actions.PHONE_DETAIL_FETCH_ERROR, error: e, phone: {} });
  }
}

function* watchFetchDetailRequest() {
  yield takeLatest(actions.PHONE_DETAIL_FETCH_REQUEST, fetchPhoneDetail);
}
export default watchFetchDetailRequest;
