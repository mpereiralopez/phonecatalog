import { actions } from './../constants';

export const fetchPhoneDetail = (id) => ({
  type: actions.PHONE_DETAIL_FETCH_REQUEST,
  id,
});
