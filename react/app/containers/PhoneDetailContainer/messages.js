/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.PhoneDetailContainer.header',
    defaultMessage: 'Detalles del Dispositivo',
  },
  loading: {
    id: 'app.components.PhoneDetailContainer.loading',
    defaultMessage: 'Cargando detalles del dispositivo seleccionado',
  },
  deviceName: {
    id: 'app.components.deviceName',
    defaultMessage: 'Device Name',
  },
  brand: {
    id: 'app.components.brand',
    defaultMessage: 'Brand',
  },
  os: {
    id: 'app.components.os',
    defaultMessage: 'OS',
  },
  dimensions: {
    id: 'app.components.dimensions',
    defaultMessage: 'Dimensions',
  },
  weight: {
    id: 'app.components.weight',
    defaultMessage: 'Weight',
  },
  sim: {
    id: 'app.components.sim',
    defaultMessage: 'Sim',
  },
  resolution: {
    id: 'app.components.resolution',
    defaultMessage: 'Resolution',
  },
  technology: {
    id: 'app.components.technology',
    defaultMessage: 'Technology',
  },
  wlan: {
    id: 'app.components.wlan',
    defaultMessage: 'Wlan',
  },
  bluetooth: {
    id: 'app.components.bluetooth',
    defaultMessage: 'Bluetooth',
  },
  usb: {
    id: 'app.components.usb',
    defaultMessage: 'Usb',
  },
  battery_c: {
    id: 'app.components.battery_c',
    defaultMessage: 'Battery',
  },
  primary_: {
    id: 'app.components.primary_',
    defaultMessage: 'Primary Camera',
  },
  video: {
    id: 'app.components.video',
    defaultMessage: 'Video',
  },
  secondary: {
    id: 'app.components.secondary',
    defaultMessage: 'Secondary Camera',
  },
  speed: {
    id: 'app.components.speed',
    defaultMessage: 'Speed',
  },
  network_c: {
    id: 'app.components.network_c',
    defaultMessage: 'Network',
  },
  chipset: {
    id: 'app.components.chipset',
    defaultMessage: 'Chipset',
  },
  alert_message: {
    id: 'app.components.alert_message',
    defaultMessage: 'Dispositivo no encontrado',
  },
  alert_description: {
    id: 'app.components.alert_description',
    defaultMessage: 'El dispostivo que esta solicitando no se encuentra disponible',
  },
});
