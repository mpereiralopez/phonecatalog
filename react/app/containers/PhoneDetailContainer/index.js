/*
 * PhoneDetail
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { compose } from 'redux';
import { Layout, Alert } from 'antd';
import { connect } from 'react-redux';
// import immutable from 'immutable';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import messages from './messages';
import reducer from './reducer';
import saga from './saga';
import { fetchPhoneDetail } from './actions';
import { CONFIG_REDUCER_KEY, CONFIG_SAGA_KEY } from './../constants';
import PhoneDetailComponent from './../../components/PhoneDetail';
import LoadingSpinner from './../../components/LoadingSpinner';

const { Header, Content } = Layout;

class PhoneDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    const { match } = this.props;
    const { id } = match.params;
    this.props.onFetchPhoneDetail(id);
  }

  shouldComponentUpdate(nextProps) {
    if (!this.props.loading && nextProps.loading) return true;
    if (nextProps.phone && this.props.phone !== nextProps.phone) return true;
    if (nextProps.hasError) return true;
    return false;
  }

  render() {
    const labels = { ...messages };
    delete labels.header;
    return (
      <Layout>
        <Header>
          <h1 style={{ color: 'white' }}>
            <FormattedMessage {...messages.header} />
          </h1>
        </Header>
        <Content style={{ margin: '15px' }}>
          {
            this.props.loading && <LoadingSpinner {...messages.loading} />
          }
          {this.props.hasError && <Alert type="error" message={this.props.errorMsg} banner closable showIcon />}
          {
            !this.props.loading &&
            <PhoneDetailComponent labels={labels} {...this.props.phone} />
          }
        </Content>
      </Layout>

    );
  }
}

PhoneDetail.propTypes = {
  onFetchPhoneDetail: PropTypes.func.isRequired,
  phone: PropTypes.object,
  match: PropTypes.object,
  hasError: PropTypes.bool,
  loading: PropTypes.bool,
  errorMsg: PropTypes.string,
};

function mapDispatchToProps(dispatch) {
  return {
    onFetchPhoneDetail: (id) => dispatch(fetchPhoneDetail(id)),
  };
}

function mapStateToProps(state) {
  return {
    loading: state.get(CONFIG_REDUCER_KEY).get('loading'),
    hasError: state.get(CONFIG_REDUCER_KEY).get('hasError'),
    errorMsg: state.get(CONFIG_REDUCER_KEY).get('errorMsg'),
    phone: state.get(CONFIG_REDUCER_KEY).get('phone'),
  };
}


const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: CONFIG_REDUCER_KEY, reducer });
const withSaga = injectSaga({ key: CONFIG_SAGA_KEY, saga, mode: '@@saga-injector/daemon' });

export default compose(
  withSaga,
  withReducer,
  withConnect
)(PhoneDetail);
