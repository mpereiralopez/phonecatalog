import { fromJS } from 'immutable';
import reducer from '../reducer';
import { actions } from './../../constants';

describe('Tests reducer', () => {
  const initialState = fromJS({ phone: {}, loading: true, hasError: false, errorMsg: '' });
  const mockPhone = {
    id: 1,
    brand: 'mockBrand',
  };

  const mockErrorMsg = 'This is a mock error message';

  it('should return the initial state', () => {
    expect(reducer(undefined, {}).toJS()).toEqual(initialState.toJS());
  });

  it('should handle Fetch phone List request action', () => {
    const action = { type: actions.PHONE_DETAIL_FETCH_REQUEST, id: 1 };
    const loading = reducer(initialState, action).get('loading');
    const phone = reducer(initialState, action).get('phone');
    expect(loading).toEqual(true);
    expect(phone).toEqual({});
  });

  it('should handle Fetch phone List success action', () => {
    const action = { type: actions.PHONE_DETAIL_FETCH_SUCCESS, phone: mockPhone };
    const loading = reducer(initialState, action).get('loading');
    const hasError = reducer(initialState, action).get('hasError');
    const phone = reducer(initialState, action).get('phone');
    expect(loading).toEqual(false);
    expect(hasError).toEqual(false);
    expect(phone).toEqual(mockPhone);
  });
  it('should handle Fetch phone List Error action', () => {
    const action = { type: actions.PHONE_DETAIL_FETCH_ERROR, error: mockErrorMsg };
    const loading = reducer(initialState, action).get('loading');
    const hasError = reducer(initialState, action).get('hasError');
    const phone = reducer(initialState, action).get('phone');
    const errorMsg = reducer(initialState, action).get('errorMsg');
    expect(loading).toEqual(false);
    expect(hasError).toEqual(true);
    expect(errorMsg).toEqual(mockErrorMsg);
    expect(phone).toEqual({});
  });
});

