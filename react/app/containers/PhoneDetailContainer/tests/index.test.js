import React from 'react';
import { FormattedMessage } from 'react-intl';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import PhoneDetail from '../index';
import messages from '../messages';


configure({ adapter: new Adapter() });

describe('<PhoneDetail />', () => {
  it('should render the page message', () => {
    const renderedComponent = shallow(
      <PhoneDetail />
    );
    expect(renderedComponent.contains(
      <FormattedMessage {...messages.header} />
    )).toEqual(true);
  });
});
