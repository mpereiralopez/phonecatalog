import { fromJS } from 'immutable';
import { actions } from './../constants';

const initialState = fromJS({ phonesList: [], loading: true, hasError: false, errorMsg: '' });


export default function (state = initialState, action) {
  switch (action.type) {
    case actions.PHONE_LIST_FETCH_REQUEST:
      return state.set('loading', true).set('phoneList', []);
    case actions.PHONE_LIST_FETCH_SUCCESS:
      return state.set('loading', false).set('phoneList', action.phoneList);
    case actions.PHONE_LIST_FETCH_ERROR:
      return state.set('loading', false).set('phoneList', []).set('hasError', true).set('errorMsg', `${action.error}`);
    default:
      return state;
  }
}
