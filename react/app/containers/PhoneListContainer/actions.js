import { actions } from './../constants';

export const fetchPhoneList = () => ({
  type: actions.PHONE_LIST_FETCH_REQUEST,
  loading: true,
});
