/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { compose } from 'redux';
import { List, Layout, Alert } from 'antd';
import { connect } from 'react-redux';
// import immutable from 'immutable';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import messages from './messages';
import reducer from './reducer';
import saga from './saga';
import { fetchPhoneList } from './actions';
import { CONFIG_REDUCER_KEY, CONFIG_SAGA_KEY } from './../constants';
import PhoneCard from './../../components/PhoneCard';
import LoadingSpinner from './../../components/LoadingSpinner';

const { Header, Content } = Layout;

class HomePage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.viewPhoneDetail = this.viewPhoneDetail.bind(this);
  }

  componentDidMount() {
    this.props.onFetchPhoneList();
  }

  shouldComponentUpdate(nextProps) {
    if (!this.props.loading && nextProps.loading) return true;
    if (nextProps.phoneList.length !== 0 && this.props.phoneList !== nextProps.phoneList) return true;
    if (nextProps.hasError) return true;
    return false;
  }

  viewPhoneDetail(id) {
    this.props.history.push(`/phones/${id}`);
  }

  render() {
    const labels = { ...messages };
    delete labels.header;
    return (
      <Layout style={{ height: '100vh' }}>
        <Header>
          <h1 style={{ color: 'white' }}>
            <FormattedMessage {...messages.header} />
          </h1>
        </Header>
        <Content style={{ margin: '15px' }}>
          {
            this.props.loading && <LoadingSpinner {...messages.loading} />
          }
          {
            this.props.hasError && <Alert type="error" message={this.props.errorMsg} banner closable showIcon />
          }
          {!this.props.loading &&
            <List
              grid={{ gutter: 16, xs: 1, sm: 2, md: 4, lg: 4, xl: 6, xxl: 3 }}
              dataSource={this.props.phoneList}
              renderItem={(item) => (
                <List.Item>
                  <PhoneCard labels={labels} {...item} viewPhoneDetail={this.viewPhoneDetail} />
                </List.Item>
              )}
            />}
        </Content>
      </Layout>

    );
  }
}

HomePage.propTypes = {
  onFetchPhoneList: PropTypes.func.isRequired,
  phoneList: PropTypes.arrayOf(PropTypes.object),
  history: PropTypes.object,
  hasError: PropTypes.bool,
  loading: PropTypes.bool,
  errorMsg: PropTypes.string,
};

function mapDispatchToProps(dispatch) {
  return {
    onFetchPhoneList: () => dispatch(fetchPhoneList()),
  };
}

function mapStateToProps(state) {
  return {
    loading: state.get(CONFIG_REDUCER_KEY).get('loading'),
    hasError: state.get(CONFIG_REDUCER_KEY).get('hasError'),
    errorMsg: state.get(CONFIG_REDUCER_KEY).get('errorMsg'),
    phoneList: state.get(CONFIG_REDUCER_KEY).get('phoneList'),
  };
}


const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: CONFIG_REDUCER_KEY, reducer });
const withSaga = injectSaga({ key: CONFIG_SAGA_KEY, saga, mode: '@@saga-injector/daemon' });

export default compose(
  withSaga,
  withReducer,
  withConnect
)(HomePage);
