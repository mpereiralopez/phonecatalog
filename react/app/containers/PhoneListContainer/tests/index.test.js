import React from 'react';
import { FormattedMessage } from 'react-intl';
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import configureStore from './../../../configureStore';

import HomePage from '../index';
import LoadingSpinner from './../../../components/LoadingSpinner';
import messages from '../messages';

configure({ adapter: new Adapter() });


// const initialState = { phonesList: [], loading: true, hasError: false, errorMsg: '' };

describe('<HomePage />', () => {
  let store;
  beforeAll(() => {
    store = configureStore({}, {});
  });

  it('should loading spinner', () => {
    const renderedComponent = mount(
      <Provider store={store}>
        <LoadingSpinner {...messages.loading} />
      </Provider>
    );
    expect(renderedComponent.contains(
      <LoadingSpinner {...messages.loading} />
    )).toEqual(true);
  });


  it('should render the page message', () => {
    const renderedComponent = shallow(
      <HomePage />
    );
    expect(renderedComponent.contains(
      <FormattedMessage {...messages.header} />
    )).toEqual(true);
  });
});
