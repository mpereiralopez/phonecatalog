import { fromJS } from 'immutable';
import reducer from '../reducer';
import { actions } from './../../constants';

describe('Tests reducer', () => {
  const initialState = fromJS({ phonesList: [], loading: true, hasError: false, errorMsg: '' });
  const mockPhoneList = [{
    id: 1,
    brand: 'mockBrand',
  },
  {
    id: 2,
    brand: 'mockBrand2',
  }];

  const mockErrorMsg = 'This is a mock error message';

  it('should return the initial state', () => {
    expect(reducer(undefined, {}).toJS()).toEqual(initialState.toJS());
  });

  it('should handle Fetch phone List request action', () => {
    const action = { type: actions.PHONE_LIST_FETCH_REQUEST, id: 1 };
    const loading = reducer(initialState, action).get('loading');
    const phoneList = reducer(initialState, action).get('phoneList');
    expect(loading).toEqual(true);
    expect(phoneList).toHaveLength(0);
  });

  it('should handle Fetch phone List success action', () => {
    const action = { type: actions.PHONE_LIST_FETCH_SUCCESS, phoneList: mockPhoneList };
    const loading = reducer(initialState, action).get('loading');
    const hasError = reducer(initialState, action).get('hasError');
    const phoneList = reducer(initialState, action).get('phoneList');
    expect(loading).toEqual(false);
    expect(hasError).toEqual(false);
    expect(phoneList).toHaveLength(2);
  });
  it('should handle Fetch phone List Error action', () => {
    const action = { type: actions.PHONE_LIST_FETCH_ERROR, error: mockErrorMsg };
    const loading = reducer(initialState, action).get('loading');
    const hasError = reducer(initialState, action).get('hasError');
    const phoneList = reducer(initialState, action).get('phoneList');
    const errorMsg = reducer(initialState, action).get('errorMsg');
    expect(loading).toEqual(false);
    expect(hasError).toEqual(true);
    expect(errorMsg).toEqual(mockErrorMsg);
    expect(phoneList).toHaveLength(0);
  });
});

