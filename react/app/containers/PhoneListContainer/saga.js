import { takeLatest, call, put, take } from 'redux-saga/effects';
import axios from 'axios';
import { actions, CONN } from './../constants';

const { API_BASE_URL, API_PORT, API_VERSION, PHONES } = CONN;

export function* fetchPhonesList() {
  try {
    const results = yield call(fetchFromApi);
    yield put({ type: actions.PHONE_LIST_FETCH_SUCCESS, phoneList: results });
  } catch (e) {
    yield put({ type: actions.PHONE_LIST_FETCH_ERROR, error: e, phoneList: [] });
  }
}

const fetchFromApi = async () => {
  const url = `${API_BASE_URL}:${API_PORT}/${API_VERSION}/${PHONES}`;
  try {
    const response = await axios.get(url);
    const responseBody = response.data;
    return responseBody;
  } catch (error) {
    throw error;
  }
};

function* watchFetchListRequest() {
  yield takeLatest(actions.PHONE_LIST_FETCH_REQUEST, fetchPhonesList);
  yield take('LOCATION_CHANGE');
}
export default watchFetchListRequest;
