/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.HomePage.header',
    defaultMessage: 'Lista de Dispositivos',
  },
  loading: {
    id: 'app.components.HomePage.loading',
    defaultMessage: 'Cargando datos de los dispositivos',
  },
  deviceName: {
    id: 'app.components.deviceName',
    defaultMessage: 'Device Name',
  },
  brand: {
    id: 'app.components.brand',
    defaultMessage: 'Brand',
  },
  os: {
    id: 'app.components.os',
    defaultMessage: 'OS',
  },
  dimensions: {
    id: 'app.components.dimensions',
    defaultMessage: 'Dimensions',
  },
  weight: {
    id: 'app.components.weight',
    defaultMessage: 'Weight',
  },
  sim: {
    id: 'app.components.sim',
    defaultMessage: 'Sim',
  },
  resolution: {
    id: 'app.components.resolution',
    defaultMessage: 'Resolution',
  },
});
