# REACT WEB APP

This file contain a description of the React app

## Features

The react app is ready to be used in production. Highly reusable, extensible, and "Localization" ready.

This project works with

- ES2017 latest features like Async/Await
- Linting with [eslint](http://eslint.org)
- Redux
- ImmutableJS
- redux-saga
- i18n
- routing

For further detailes (libraries, scripts, etc), please check the `package.json` file of the project