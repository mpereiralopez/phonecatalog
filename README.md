# Phone List Challenge

This is the Readme file for the phone list challenge.

## Project structure
 - **api-server**: contains the api microservice in RESTFULL standar. for more detail please reffer to the  [API Readme](api-server/README.md)

 - **react**: contains treact application. for more detail please reffer to the  [React Readme](react/README.md)

- **.env**: Enviroment file with the content of the enviroment to run, and ports for api and app

```javascript
NODE_ENV=development
API_PORT=3000
APP_PORT=3002
```

- **docker-compose.yml**: Docker compose file to build app and api.

## Execution
 
 Execute the application by running `cd phonecatalog && docker-compose up`
 Note: Tested on version `docker-compose version 1.21.1`