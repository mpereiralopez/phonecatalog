module.exports = {
  env: process.env.NODE_ENV || 'dev',
  port: process.env.API_PORT || 3000,
  defaultLocale: 'en',
  logs: process.env.NODE_ENV === 'production' ? 'combined' : 'dev',
};
