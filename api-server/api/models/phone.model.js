const devices = require('./fake_db.json');

/*
const fields = ['_id', 'imgs', 'deviceName', 'brand', 'technology', 'gprs', 'edge',
  'announced', 'status', 'dimensions', 'weight', 'sim', 'type', 'resolution', 'card_slot',
  'phonebook', 'call_records', 'alert_types', 'loudspeaker', 'wlan', 'bluetooth', 'gps', 'radio',
  'usb', 'messaging', 'browser', 'games', 'java', 'features_c', 'battery_c', 'stand_by',
  'talk_time', 'colors', 'sar_eu', 'internal', 'primary_', 'video', 'secondary', 'speed',
  'features', '_2g_bands', '_3_5mm_jack_', '_3g_bands'];
  */


const Phone = {
  list: (params) => {
    const { os, brand } = params;
    let filtered = [...devices];
    if (os) filtered = filtered.filter((dev) => dev.os.toLowerCase().indexOf(os) !== -1);
    if (brand) filtered = filtered.filter((dev) => dev.brand.toLowerCase().indexOf(brand) !== -1);
    return filtered;
  },
  get: (id) => devices.find((dev) => dev.id === parseInt(id, 10)),

};

module.exports = Phone;
