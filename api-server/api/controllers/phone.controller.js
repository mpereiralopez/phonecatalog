const Phone = require('../models/phone.model');

/**
 * Load user and append to req.
 * @public
 */
exports.get = (req, res, next) => {
  try {
    const { id } = req.params;
    const phone = Phone.get(id);
    // setTimeout((() => { res.send(phone); }), 3000);
    res.send(phone);
  } catch (error) {
    next(error);
  }
};

/**
 * Get user list
 * @public
 */
exports.list = (req, res, next) => {
  try {
    const phones = Phone.list(req.query);
    // setTimeout((() => { res.send(phones); }), 3000);
    res.send(phones);
  } catch (error) {
    next(error);
  }
};
