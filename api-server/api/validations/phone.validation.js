const Joi = require('joi');

module.exports = {

  // GET /v1/users
  listPhones: {
    query: {
      page: Joi.number().min(1),
      perPage: Joi.number().min(1).max(100),
      os: Joi.string(),
      brand: Joi.string(),
    },
  },
};
